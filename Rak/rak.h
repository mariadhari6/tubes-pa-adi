#include<iostream>
#include<curses.h>
#include<string>

#ifndef RAK_LIBRARY
#define RAK_LIBRARY

using namespace std;
typedef struct elemen_rak *address1;
struct rak
{
    string kode_rak;
    int isi_max, isi, id_rak;
};
struct elemen_rak
{
    rak info;
    address1 next;
};
struct list_rak
{
    address1 mulai;
};
string dataRak;
string rak_code[6]={"A", "B", "C", "D", "E", "F"};
int Pilihan_Rak();
address1 Alokasi_Rak(list_rak data_rak, string Kode_Rak, int Isi_Max,
                     int index_rak, int value_index, int number);
void Buat_Data_Rak(list_rak Data);
void Tampilkan_Data_Rak(list_rak l);
int PilihTampilkan();
list_rak Tambah_Rak_Awal(list_rak l, address1 data_list);
list_rak Tambah_Rak_Akhir(list_rak l, address1 data_list);
string InputKodeRak();
void Cari_Rak(list_rak l, string kode_rak);
list_rak data_rak;
#endif