#include "rak.h"
void Buat_Data_Rak(list_rak Data){
    Data.mulai = NULL;
}

int Pilihan_Rak(){
    int pilih;
    cout<<"1. Tampilkan Data Rak\n2. Tambah Rak\n3. Cari Rak ";
    cout<<"\n4. Kembali\n  Pilih\t: ";
    cin>>dataRak;
    try
    {
        pilih = stoi(dataRak);
    }
    catch(const std::exception& e)
    {
        pilih = 0;
    }
    return pilih;
}
int InputIsiMax(){
    string kapasitas;
    int kp;
    do
    {
        cout<<"Kapasitas Rak : ";
        cin>>kapasitas;
        try
        {
            kp = stoi(kapasitas);
        }
        catch(const std::exception& e)
        {
            kp = 0;
        }
    } while (kp<=0 || kp>150);
    
    return kp;
}

address1 Alokasi_Rak(list_rak data_rak, string Kode_Rak, int Isi_Max,
                     int index_rak, int value_index, int number){

    address1 a =new elemen_rak;
    string value_index_str;
    value_index_str = to_string(value_index);
    Kode_Rak = rak_code[index_rak]; 
    a->info.id_rak = number;
    Kode_Rak = Kode_Rak + value_index_str;
    a->info.kode_rak = Kode_Rak;
    a->info.isi_max = Isi_Max;
    a->info.isi = 0;
    a->next = NULL;
    return a;
}

list_rak Tambah_Rak_Awal(list_rak l, address1 data_list){
    if (l.mulai == NULL)
    {
        l.mulai = data_list;
        return l;
    }
    else
    {
        data_list->next = l.mulai;
        l.mulai = data_list;
        data_list = NULL;
        return l;
    }
}
list_rak Tambah_Rak_Akhir(list_rak l, address1 data_list){
    address1 a = l.mulai;
    while (a->next!=NULL)
    {
        a=a->next;
    }
    a->next=data_list;
    data_list=NULL;
    return l;
}

void Tampilkan_Data_Rak(list_rak l){
    if(l.mulai ==  NULL){
        cout<<"Tidak Ada Rak Yang Terdaftar!!!\n";
        return;
    }
    address1 a = l.mulai;
    while (a!=NULL)
    {
        cout<<"=====Data Rak Ke-"<<a->info.id_rak<<"=====\n";
        cout<<"Kode Rak\t= "<<a->info.kode_rak<<endl;
        cout<<"Isi Rak\t\t= "<<a->info.isi<<endl;
        cout<<"Kapasitas\t= "<<a->info.isi_max<<endl<<endl;
        a = a->next;
    }    
}
string InputKodeRak(){
    system("clear");
    cin.get();
    char Rak[100];
    cout<<"Cari Rak\t: ";
    cin.get(Rak, 90);
    dataRak = Rak;
    return dataRak;
}
void Cari_Rak(list_rak l, string kode_rak){
    system("clear");
    address1 a = l.mulai;
    while (a!=NULL)
    {
        if (a->info.kode_rak.compare(kode_rak) == 0)
        {
            cout<<"=====Data Rak"<<"=====\n";
            cout<<"Kode Rak\t= "<<a->info.kode_rak<<endl;
            cout<<"Isi Rak\t\t= "<<a->info.isi<<endl;
            cout<<"Kapasitas\t= "<<a->info.isi_max<<endl<<endl;
            return;
        }
        a = a->next;
    }
    cout<<"Kode Rak Tidak Ditemukan !!!\n";
}