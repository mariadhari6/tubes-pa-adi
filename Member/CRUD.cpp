#include "member.h"

address3 Alokasi_Member(string _nim, string _nama,
                 string _jk, string _alamat,
                 string _no_telp, string _kelas,
                 int _thn_masuk){

    address3 a = new elemen_anggota;
    a->info.nim_anggota = _nim;
    a->info.nama_anggota = _nama;
    a->info.jk_anggota = _jk;
    a->info.no_telp = _no_telp;
    a->info.kelas = _kelas;
    a->info.alamat =_alamat;
    a->info.thn_masuk = _thn_masuk;
    a->next = NULL;
    return a;
}

list_anggota Tambah_Awal_Anggota(list_anggota l, address3 data_list){
    if (l.mulai==NULL)
    {
        l.mulai=data_list;
        return l;
    }
    else
    {
        data_list->next=l.mulai;
        l.mulai=data_list;
        data_list=NULL;
        return l;
    }
}
list_anggota Tambah_Akhir_Anggota(list_anggota l, address3 data_list){
    address3 a = l.mulai;
    while (a->next!=NULL)
    {
        a = a->next;
    }
    a->next = data_list;
    data_list = NULL;
    return l;
}

list_anggota Delete_Anggota(list_anggota l, string nim_tujuan){
    if (l.mulai == NULL)
    {
        cout<<"Data Anggota Perpustakaan Masih Kosong !!!\n";
        return l;
    }
    address3 wadah;
    address3 a = l.mulai;
    if (l.mulai->info.nim_anggota.compare(nim_tujuan) == 0)
    {
        wadah = l.mulai;
        l.mulai = l.mulai->next;
        return l;
    }
    while (a->next!=NULL)
    {
        if (a->next->info.nim_anggota.compare(nim_tujuan) == 0)
        {
            wadah = a->next;
            a->next = wadah->next;
            return l;
        }
        a = a->next;
    }
}
list_anggota Delete_Anggota_Setelah(list_anggota l, string nim_tujuan){
    if (l.mulai == NULL)
    {
        cout<<"Data Anggota Perpustakaan Masih Kosong !!!\n";
        return l;
    }
    address3 wadah;
    address3 a = l.mulai;
    if (l.mulai->info.nim_anggota.compare(nim_tujuan) == 0)
    {
        if (l.mulai->next == NULL)
        {
            system("clear");
            cout<<"Data Setelah NIM "<<nim_tujuan<<" Masih Kosong !!!\n";
            return l;
        }
        wadah = a->next;
        l.mulai->next = wadah->next;
        return l;
    }
    while (a->next!=NULL)
    {
        if (a->info.nim_anggota.compare(nim_tujuan) == 0)
        {
            wadah = a->next;
            a->next = wadah->next;
            return l;
        }
        a = a->next;
    }
    system("clear");
    cout<<"Data Setelah NIM "<<nim_tujuan<<" Masih Kosong !!!\n";
    return l;
}

void Cari_Anggota(list_anggota l, string nim_tujuan){
    system("clear");
    address3 a = l.mulai;
    while (a!=NULL)
    {
        if (a->info.nim_anggota.compare(nim_tujuan) == 0)
        {
            cout<<"=====Data Buku"<<"=====\n";
            cout<<"NIM\t= "<<a->info.nim_anggota<<endl;
            cout<<"Nama\t= "<<a->info.nama_anggota<<endl;
            cout<<"Kelas\t= "<<a->info.kelas<<endl;
            cout<<"Jenis Kelamin= "<<a->info.jk_anggota<<endl;
            cout<<"Alamat\t= "<<a->info.alamat<<endl;
            cout<<"NO Telp\t= "<<a->info.no_telp<<endl;
            cout<<"Tahun Masuk= "<<a->info.thn_masuk<<endl<<endl;
            return;
        }
        a = a->next;
    }    
}

void Hitung_Anggota(list_anggota l){
    int jml = 0;
    address3 a = l.mulai;
    while (a!=NULL)
    {
        a = a->next;
        jml++;
    }
    cout<<"Jumlah Anggota Perpustakaan : "<<jml<<endl;   
}

list_anggota Tambah_Setelah_Anggota(list_anggota l, address3 data_list, string nim_tujuan){
    if (l.mulai!=NULL)
    {
        address3 a = l.mulai;
        while (a!=NULL)
        {
            if (a->info.nim_anggota.compare(nim_tujuan) == 0)
            {
                data_list->next = a->next;
                a->next = data_list;
                return l;
            }
            a = a->next;
        }
    }
}

list_anggota Update_Data_Anggota(list_anggota l, address3 data_list, string nim_tujuan){
    address3 a = l.mulai;
    while (a!=NULL)
    {
        if (a->info.nim_anggota.compare(nim_tujuan) == 0)
        {
            a->info.nama_anggota = data_list->info.nama_anggota;
            a->info.jk_anggota = data_list->info.jk_anggota;
            a->info.no_telp = data_list->info.no_telp;
            a->info.alamat = data_list->info.alamat;
            a->info.thn_masuk = data_list->info.thn_masuk;
            return l;
        }
        a = a->next;
    }
}

void Tampilkan_Data_Anggota(list_anggota l){
    if (l.mulai == NULL)
    {
        cout<<"Data Anggota Kosong !!!\n";
        return;
    }
    address3 a = l.mulai;
    int x = 1;
    while (a!=NULL)
    {
        cout<<"=====Data Anggota Ke-"<<x<<"=====\n";
        cout<<"NIM\t= "<<a->info.nim_anggota<<endl;
        cout<<"Nama\t= "<<a->info.nama_anggota<<endl;
        cout<<"Kelas\t= "<<a->info.kelas<<endl;
        cout<<"Jenis Kelamin= "<<a->info.jk_anggota<<endl;
        cout<<"Alamat\t= "<<a->info.alamat<<endl;
        cout<<"NO Telp\t= "<<a->info.no_telp<<endl;
        cout<<"Tahun Masuk= "<<a->info.thn_masuk<<endl<<endl;
        x++;
        a = a->next;
    }      
}

void Cari_Nama(list_anggota l, string nama){
    address3 a = l.mulai;
    int x = 1;
    while (a!=NULL)
    {
        if (a->info.nama_anggota.compare(nama) == 0)
        {
            cout<<"=====Data Anggota Ke-"<<x<<"=====\n";
            cout<<"NIM\t= "<<a->info.nim_anggota<<endl;
            cout<<"Nama\t= "<<a->info.nama_anggota<<endl;
            cout<<"Kelas\t= "<<a->info.kelas<<endl;
            cout<<"Jenis Kelamin= "<<a->info.jk_anggota<<endl;
            cout<<"Alamat\t= "<<a->info.alamat<<endl;
            cout<<"NO Telp\t= "<<a->info.no_telp<<endl;
            cout<<"Tahun Masuk= "<<a->info.thn_masuk<<endl<<endl;
            x++;
        }       
        a = a->next;
    }
}

void Cari_Kelas(list_anggota l, string kelas){
    address3 a = l.mulai;
    int x = 1;
    while (a!=NULL)
    {
        if (a->info.kelas.compare(kelas) == 0)
        {
            cout<<"=====Data Anggota Ke-"<<x<<"=====\n";
            cout<<"NIM\t= "<<a->info.nim_anggota<<endl;
            cout<<"Nama\t= "<<a->info.nama_anggota<<endl;
            cout<<"Kelas\t= "<<a->info.kelas<<endl;
            cout<<"Jenis Kelamin= "<<a->info.jk_anggota<<endl;
            cout<<"Alamat\t= "<<a->info.alamat<<endl;
            cout<<"NO Telp\t= "<<a->info.no_telp<<endl;
            cout<<"Tahun Masuk= "<<a->info.thn_masuk<<endl<<endl;
            x++;
        }
        a = a->next;
    }
}