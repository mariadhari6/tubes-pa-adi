#include "member.h"

void Buat_Data_Anggota(list_anggota Data){
    Data.mulai = NULL;
}

string InputNim(){
    string nim;
    int nim_int;
    do
    {
        cout<<"NIM\t: ";
        cin>>nim;
        try
        {
            nim_int = stoi(nim);
        }
        catch(const std::exception& e)
        {
            nim_int = 0;
        }
        if (nim_int == 0)
        {
            cout<<"Nim Salah!!!\n";
        }
    } while (nim_int == 0);
    nim.resize(7);
    return nim;
}

string InputNama(){
    string nama;
    cin.get();
    cout<<"Nama\t: ";
    cin.get(Inputan, 90);
    nama = Inputan;
    return nama;
}

string InputJKAnggota(){
    int i;
    string jk;
    do
    {
        cout<<"Jenis Kelamin\t: \n";
        for (i = 0; i < 2; i++)
        {
            cout<<"\t\t: "<<i+1<<". "<<jenis_kelamin[i]<<endl;
        }
        cout<<"  Pilih: ";
        cin>>jk;
        try
        {
            i = stoi(jk);
        }
        catch(const std::exception& e)
        {
            i = 0;
        }
    } while (i<1 || i>2);
    jk = jenis_kelamin[i-1];
    return jk;
}

string InputAlamat(){
    string alamat;
    cin.get();
    cout<<"Alamat\t: ";
    cin.get(Inputan, 90);
    alamat = Inputan;
    return alamat;
}

int InputTahunMasuk(){
    string thn_masuk;
    int _thn_masuk;
    do
    {
        cout<<"Tahun Masuk\t: ";
        cin>>thn_masuk;
        try
        {
            _thn_masuk = stoi(thn_masuk);
        }
        catch(const std::exception& e)
        {
            _thn_masuk = 0;
        }
    } while (_thn_masuk<2008 || _thn_masuk>2020);
    return _thn_masuk;
}
string InputNoHP(){
    string no_hp;
    char _no_hp[100];
    
    int hp_int, i=0;
    bool salah = false;
    string test;
    do
    {
        cin.get();
        salah = false;
        cout<<"No HP\t: ";
        cin.get(_no_hp, 90);
        no_hp = _no_hp;
        while (i<no_hp.size())
        {
            test = _no_hp[i];
            try
            {
                hp_int = stoi(test);
            }
            catch(const std::exception& e)
            {
                salah = true;
            }
            if (salah == true)
            {
                break;
            }
            
            i++;
        }
    } while (salah == true);
    
    
    cout<<endl;
    no_hp.resize(13);
    return no_hp;
}

string InputKelas(){
    string kelas;
    cin.get();
    cout<<"Kelas\t: ";
    cin.get(Inputan, 90);
    kelas = Inputan;
    return kelas;
}

int Pilihan_Anggota(){
    string pilih;
    int _pilih;
    do
    {
        cout<<"1.Tampilkan Data Anggota\n2.Tambah Anggota Diawal\n3.Tambah ";
        cout<<"Anggota Diakhir\n4.Delete Anggota\n5.Cari Anggota\n6.Hitung ";
        cout<<"Jumlah Anggota\n7.Tambah Anggota Setelah\n8.Update Data Anggota\n";
        cout<<"9.Delete Setelah\n10.Kembali\n  Pilih\t: ";
        cin>>pilih;
        try
        {
            _pilih = stoi(pilih);
        }
        catch(const std::exception& e)
        {
            _pilih = 0;
        }
    } while (_pilih<=0 || _pilih>10);
    return _pilih;
}

///Validasi
bool CekNim(list_anggota l, string Nim){
    bool ada = false;
    address3 a =l.mulai;
    int b;
    while (a!=NULL)
    {
        b = Nim.compare(a->info.nim_anggota);
        if (b==0)
        {
            ada = true;
            break;
        }
        a = a->next;
    }
    return ada;
}
bool CekNama(list_anggota l, string nama){
    address3 a = l.mulai;
    while (a!=NULL)
    {
        if (a->info.nama_anggota.compare(nama) == 0)
        {
            return true;
        }
        a = a->next;
    }
    return false;
}
bool CekKelas(list_anggota l, string kelas){
    address3 a = l.mulai;
    while (a!=NULL)
    {
        if (a->info.kelas.compare(kelas) == 0)
        {
            return true;
        }
        a = a->next;
    }
    return false;
}