#include<iostream>
#include<curses.h>
#include<string>

#ifndef MEMBER_LIBRARY
#define MEMBER_LIBRARY


using namespace std;
typedef struct elemen_anggota *address3;
struct anggota
{
    string nim_anggota, nama_anggota, jk_anggota;
    string alamat, no_telp, kelas;
    int thn_masuk;
};
struct elemen_anggota
{
    anggota info;
    address3 next;
};
struct list_anggota
{
    address3 mulai;
};
string jenis_kelamin[2] = {
    "Laki-laki",
    "Perempuan"
};
list_anggota data_anggota;
void Buat_Data_Anggota(list_anggota Data);
int Pilihan_Anggota();
string InputNim();
string InputNama();
string InputJKAnggota();
string InputAlamat();
string InputNoHP();
string InputKelas();
int InputTahunMasuk();
bool CekNim(list_anggota l, string Nim);
char Inputan[100];
address3 Alokasi_Member(string _nim, string _nama,
                        string _jk, string _alamat,
                        string _no_telp, string _kelas,
                        int _thn_masuk);

list_anggota Tambah_Awal_Anggota(list_anggota l, address3 data_list);
list_anggota Tambah_Akhir_Anggota(list_anggota l, address3 data_list);
list_anggota Delete_Anggota(list_anggota l, string nim_tujuan);
list_anggota Delete_Anggota_Setelah(list_anggota l, string nim_tujuan);
void Cari_Anggota(list_anggota l, string nim_tujuan);
void Tampilkan_Data_Anggota(list_anggota l);
void Hitung_Anggota(list_anggota l);
list_anggota Tambah_Setelah_Anggota(list_anggota l, address3 data_list, string nim_tujuan);
list_anggota Update_Data_Anggota(list_anggota l, address3 data_list, string nim_tujuan);
void Cari_Nama(list_anggota l, string nama);
void Cari_Kelas(list_anggota l, string kelas);
bool CekNama(list_anggota l, string nama);
bool CekKelas(list_anggota l, string kelas);
#endif