#include "Book/CRUD.cpp"
#include "Start/Start.cpp"
#include "DataRak.cpp"
#include "DataBuku.cpp"
#include "DataAnggota.cpp"
#include "Member/CRUD.cpp"
#include "DataUser.cpp"
//Data struct rak
// list_rak data_rak;

int main() {
    Start fg;

    //Membuat Object
    DataBuku book;
    DataRak rak;
    DataAnggota member;
    DataUser user;

    //Membuat Struct
    Buat_Data_Buku(data_buku);
    Buat_Data_Rak(data_rak);
    Buat_Data_Anggota(data_anggota);

    int dipilih;
    
    address p;
    address1 q;
    address2 r;
    address3 s;
    
    //membuat data_user seeder
    user.id_petugas = getID();
    user.nama_petugas = "Admin";
    user.jk = "Laki-laki";
    user.alamat = "Indramayu";
    user.no_telp = "08976591675";
    user.username = "admin123";
    user.password = "admin123";
    r = Alokasi_User(user.id_petugas, user.nama_petugas,
                     user.jk, user.alamat, user.no_telp,
                     user.username, user.password);
    data_user = Tambah_User(data_user, r);
    
    //membuat data_rak seeder
    q = Alokasi_Rak(data_rak, rak.Kode_Rak, rak.Isi_Max,
                    rak.index_rak, rak.value_index, rak.number);
    data_rak = Tambah_Rak_Awal(data_rak, q);
    
    while (true)
    {
        system("clear");
        dipilih = fg.LoginPage();
        if (dipilih == 1)
        {
            system("clear");
            user.Input_Data_User();
            r = Alokasi_User(user.id_petugas, user.nama_petugas,
                             user.jk, user.alamat, user.no_telp,
                             user.username, user.password);
            data_user = Tambah_User(data_user, r);
        }
        else if (dipilih == 2)
        {
            system("clear");
            user.username = InputUsername();
            user.password = InputPassword();
            user.sama = CekPassword(data_user, user.username, user.password);
            if (user.sama == true)
            {
                while (true)
                {
                    system("clear");
                    dipilih = fg.Pilihan();
                    if (dipilih == 1)
                    {
                        while (true)
                        {
                            dipilih = Pilihan_Anggota();
                            if (dipilih == 1)
                            {
                                if (data_anggota.mulai == NULL)
                                {
                                    system("clear");
                                    cout<<"Data Anggota Kosong !!!\n\n";
                                    continue;
                                }
                                dipilih = member.PilihTampilkan();
                                if (dipilih == 1)
                                {
                                    system("clear");
                                    Tampilkan_Data_Anggota(data_anggota);
                                }
                                else if (dipilih == 2)
                                {
                                    system("clear");
                                    if (data_anggota.mulai == NULL)
                                    {
                                        cout<<"Data Anggota Kosong !!!\n";
                                        continue;
                                    }
                                    member.nama_anggota = InputNama();
                                    member.sama = CekNama(data_anggota, member.nama_anggota);
                                    if (member.sama == false)
                                    {
                                        system("clear");
                                        cout<<"Nama Anggota Tidak Ditemukan !!!\n";
                                        continue;
                                    }
                                    system("clear");
                                    Cari_Nama(data_anggota, member.nama_anggota);
                                }
                                else if (dipilih == 3)
                                {
                                    system("clear");
                                    member.kelas = InputKelas();
                                    member.sama = CekKelas(data_anggota, member.kelas);
                                    if (member.sama == false)
                                    {
                                        cout<<"Kelas Tidak Ditemukan !!!\n";
                                        cin.get();
                                        continue;
                                    }
                                    system("clear");
                                    Cari_Kelas(data_anggota, member.kelas);
                                }
                                else
                                {
                                    continue;
                                }
                                
                            }
                            else if (dipilih  == 2)
                            {
                                system("clear");
                                member.Input_Data();
                                s = Alokasi_Member(member.nim_anggota, member.nama_anggota,
                                                member.jk_anggota, member.alamat,
                                                member.no_telp, member.kelas, member.thn_masuk);
                                                
                                data_anggota = Tambah_Awal_Anggota(data_anggota, s);
                            }
                            else if (dipilih == 3)
                            {
                                system("clear");
                                member.Input_Data();
                                s = Alokasi_Member(member.nim_anggota, member.nama_anggota,
                                                member.jk_anggota, member.alamat,
                                                member.no_telp, member.kelas, member.thn_masuk);
                                if (data_anggota.mulai == NULL)
                                {
                                    cin.get();
                                    system("clear");
                                    data_anggota = Tambah_Awal_Anggota(data_anggota, s);
                                    continue;
                                }
                                data_anggota = Tambah_Akhir_Anggota(data_anggota, s);
                            }
                            
                            else if (dipilih == 4)
                            {
                                system("clear");
                                if (data_anggota.mulai == NULL)
                                {
                                    system("clear");
                                    cout<<"Data Anggota Perpustakaan Masih Kosong !!!\n";
                                    continue;
                                }
                                member.nim_anggota = InputNim();
                                member.sama = CekNim(data_anggota, member.nim_anggota);
                                if (member.sama == false)
                                {
                                    cout<<"Anggota Yang Dicari Tidak Ada !!!\n";
                                    continue;
                                }
                                data_anggota = Delete_Anggota(data_anggota, member.nim_anggota);
                            }
                            else if (dipilih == 5)
                            {
                                system("clear");
                                if (data_anggota.mulai == NULL)
                                {
                                    cout<<"Data Anggota Perpustakaan Masih Kosong !!!\n";
                                    continue;
                                }
                                member.nim_anggota = InputNim();
                                member.sama = CekNim(data_anggota, member.nim_anggota);
                                if (member.sama == false)
                                {
                                    cout<<"Anggota Yang Dicari Tidak Ada !!!\n";
                                    continue;
                                }
                                Cari_Anggota(data_anggota, member.nim_anggota);
                            }
                            else if (dipilih == 6)
                            {
                                system("clear");
                                Hitung_Anggota(data_anggota);
                            }
                            else if (dipilih == 7)
                            {
                                system("clear");
                                if (data_anggota.mulai == NULL)
                                {
                                    cout<<"Data Anggota Perpustakaan Masih Kosong !!!\n";
                                    continue;
                                }
                                cin.get();
                                cout<<"NIM Tujuan\t: ";
                                cin.get(Inputan, 90);
                                member.nim_tujuan = Inputan;
                                member.sama = CekNim(data_anggota, member.nim_tujuan);
                                if (member.sama == false)
                                {
                                    cout<<"Nim Yang Dicari Tidak Ada !!!\n";
                                    continue;
                                }
                                member.Input_Data();
                                s = Alokasi_Member(member.nim_anggota, member.nama_anggota,
                                                member.jk_anggota, member.alamat,
                                                member.no_telp, member.kelas, member.thn_masuk);
                                data_anggota = Tambah_Setelah_Anggota(data_anggota, s, member.nim_tujuan);
                            }
                            else if (dipilih == 8)
                            {
                                system("clear");
                                if (data_anggota.mulai == NULL)
                                {
                                    cout<<"Data Anggota Perpustakaan Masih Kosong !!!\n";
                                    continue;
                                }
                                cin.get();
                                cout<<"Nim Tujuan\t: ";
                                cin.get(Inputan, 90);
                                member.nim_tujuan = Inputan;
                                member.sama = CekNim(data_anggota, member.nim_tujuan);
                                if (member.sama == false)
                                {
                                    cout<<"Anggota Yang Dicari Tidak Ada !!!\n";
                                    continue;
                                }
                                member.Update();
                                s = Alokasi_Member(member.nim_anggota, member.nama_anggota,
                                                   member.jk_anggota, member.alamat,
                                                   member.no_telp, member.kelas, member.thn_masuk);
                                data_anggota = Update_Data_Anggota(data_anggota, s, member.nim_tujuan);
                            }
                            else if (dipilih == 9)
                            {
                                system("clear");
                                if (data_anggota.mulai == NULL)
                                {
                                    system("clear");
                                    cout<<"Data Anggota Perpustakaan Masih Kosong !!!\n";
                                    continue;
                                }
                                member.nim_anggota = InputNim();
                                member.sama = CekNim(data_anggota, member.nim_anggota);
                                if (member.sama == false)
                                {
                                    cout<<"Anggota Yang Dicari Tidak Ada !!!\n";
                                    continue;
                                }
                                data_anggota = Delete_Anggota_Setelah(data_anggota, member.nim_anggota);
                            }                
                            else if (dipilih == 10)
                            {
                                system("clear");
                                break;
                            }
                            
                        }
                    }
                    else if (dipilih == 2)
                    {
                        system("clear");
                        while (true)
                        {
                            dipilih=Pilihan_Buku();
                            if (dipilih == 1)
                            {
                                if (data_buku.mulai == NULL)
                                {
                                    system("clear");
                                    cout<<"Data Buku Masih Kosong !!!\n";
                                    continue;
                                }
                                
                                dipilih = book.PilihTampilkan();
                                if (dipilih == 1)
                                {
                                    system("clear");
                                    Tampilkan_Data(data_buku);
                                }
                                else if (dipilih == 2)
                                {
                                    system("clear");
                                    book.Jenis_Buku = InputJenisBuku();
                                    book.genre = InputGenre(book.Jenis_Buku);
                                    system("clear");
                                    Cari_Buku_Genre(data_buku, book.genre);
                                }
                                else if (dipilih == 3)
                                {
                                    system("clear");
                                    book.Penerbit = InputPenerbit();
                                    book.sama = CekPenerbit(data_buku, book.Penerbit);
                                    if (book.sama == false)
                                    {
                                        system("clear");
                                        cout<<"Penerbit Tidak Dietmukan !!!\n";
                                        continue;
                                    }
                                    system("clear");
                                    Cari_Penerbit(data_buku, book.Penerbit);
                                }
                                else if (dipilih == 4)
                                {
                                    system("clear");
                                    book.Penulis = InputPenulis();
                                    book.sama = CekPenulis(data_buku, book.Penulis);
                                    if (book.sama == false)
                                    {
                                        cout<<"Penulis Tidak Dietmukan !!!\n";
                                        continue;
                                    }
                                    system("clear");
                                    Cari_Penulis(data_buku, book.Penulis);
                                }
                                else
                                {
                                    continue;
                                }
                            }
                            else if (dipilih == 2)
                            {
                                system("clear");
                                book.Input_Data();
                                p=Alokasi(book.Judul, book.ID_Buku, book.Kode_rak,
                                          book.Thn_terbit, book.Cetakan_ke,
                                          book.Penulis, book.Penerbit, book.Kota_terbit,
                                          book.Jenis_Buku, book.Genre, book.i);
                                data_buku=Tambah_Awal(data_buku, p); 
                            }
                            else if (dipilih == 3)
                            {
                                system("clear");
                                book.Input_Data();
                                p=Alokasi(book.Judul, book.ID_Buku, book.Kode_rak,
                                          book.Thn_terbit, book.Cetakan_ke,
                                          book.Penulis, book.Penerbit, book.Kota_terbit,
                                          book.Jenis_Buku, book.Genre, book.i);
                                if (data_buku.mulai==NULL)
                                {
                                    data_buku=Tambah_Awal(data_buku, p);
                                    cin.get();
                                    system("clear");
                                    continue;
                                }
                                data_buku=Tambah_Akhir(data_buku, p);
                            }
                            else if (dipilih == 4)
                            {
                                system("clear");
                                if (data_buku.mulai==NULL)
                                {
                                    system("clear");
                                    cout<<"Data Buku Perpustakaan Masih Kosong !!!\n";
                                    continue;
                                }
                                book.id_tujuan = InputIDBuku();
                                book.sama = CekID(data_buku, book.id_tujuan);
                                if (book.sama == false)
                                {
                                    cout<<"Buku Yang Dicari Tidak Ada !!!\n";
                                    continue;
                                }
                                data_buku = Delete_Buku(data_buku, book.id_tujuan, data_rak);         
                            }
                            else if (dipilih == 5)
                            {
                                system("clear");
                                if (data_buku.mulai==NULL)
                                {
                                    cout<<"Data Buku Perpustakaan Masih Kosong !!!\n";
                                    continue;
                                }
                                book.Judul=InputJudul();
                                book.sama=CekJudul(data_buku, book.Judul);
                                if (book.sama==false)
                                {
                                    cout<<"Buku Yang Dicari Tidak Ada !!!\n";
                                    continue;
                                }
                                Cari_Buku(data_buku, book.Judul);
                            }
                            else if (dipilih == 6)
                            {
                                system("clear");
                                Hitung_Buku(data_buku);
                            }
                            else if (dipilih == 7)
                            {
                                system("clear");
                                if (data_buku.mulai==NULL)
                                {
                                    cout<<"Data Buku Perpustakaan Masih Kosong !!!\n";
                                    continue;
                                }
                                cin.get();
                                cout<<"ID Tujuan\t: ";
                                cin.get(Masukkan, 90);
                                book.id_tujuan = Masukkan;
                                book.sama = CekID(data_buku, book.id_tujuan);
                                if (book.sama == false)
                                {
                                    cout<<"Buku Yang Dicari Tidak Ada !!!\n";
                                    continue;
                                }
                                book.Input_Data();
                                p=Alokasi(book.Judul, book.ID_Buku, book.Kode_rak,
                                          book.Thn_terbit, book.Cetakan_ke,
                                          book.Penulis, book.Penerbit, book.Kota_terbit,
                                          book.Jenis_Buku, book.Genre, book.i);
                                data_buku=Tambah_Setelah(data_buku, p, book.id_tujuan);          
                            }
                            else if (dipilih == 8)
                            {
                                book.sama = false;
                                system("clear");
                                if (data_buku.mulai==NULL)
                                {
                                    cout<<"Data Buku Perpustakaan Masih Kosong !!!\n";
                                    continue;
                                }
                                cout<<"ID Tujuan\t: ";
                                cin>>book.id_tujuan;
                                book.sama=CekID(data_buku, book.id_tujuan);
                                if (book.sama==false)
                                {
                                    cout<<"Buku Yang Dicari Tidak Ada !!!\n";
                                    continue;
                                }
                                book.Update();
                                p=Alokasi(book.Judul, book.id_tujuan, book.Kode_rak,
                                          book.Thn_terbit, book.Cetakan_ke,
                                          book.Penulis, book.Penerbit, book.Kota_terbit,
                                          book.Jenis_Buku, book.Genre, book.i);
                                data_buku=Update_Data(data_buku, p, book.id_tujuan, book.i);
                            }
                            else if (dipilih == 9)
                            {
                                system("clear");
                                if (data_buku.mulai == NULL)
                                {
                                    cout<<"Data Buku Perpustakaan Masih Kosong !!!\n";
                                    continue;
                                }
                                book.id_tujuan = InputIDBuku();
                                book.sama = CekID(data_buku, book.id_tujuan);
                                if (book.sama == false)
                                {
                                    cout<<"Buku Yang Dicari Tidak Ada !!!\n";
                                    continue;
                                }
                                data_buku = Delete_Buku_Setelah(data_buku, book.id_tujuan, data_rak);
                            }
                            
                            else if (dipilih == 10)
                            {
                                system("clear");
                                break;
                            }
                        }
                    }
                    else if (dipilih == 3)
                    {
                        while (true)
                        {
                            dipilih = Pilihan_Rak();
                            if (dipilih == 1)
                            {
                                system("clear");
                                Tampilkan_Data_Rak(data_rak);
                            }
                            else if (dipilih == 2)
                            {
                                system("clear");
                                rak.number++;
                                if (rak.index_rak>=5 && rak.value_index>=6)
                                {
                                    cout<<"Data Penuh!!!\n";
                                    continue;
                                }
                                
                                if (rak.value_index>=6)
                                {
                                    rak.value_index = 1;
                                    rak.index_rak++;
                                }
                                else
                                {
                                    rak.value_index++;
                                }
                                
                                rak.Input_Data();
                                q = Alokasi_Rak(data_rak, rak.Kode_Rak, rak.Isi_Max,
                                rak.index_rak, rak.value_index, rak.number);
                                data_rak = Tambah_Rak_Akhir(data_rak, q);
                            }
                            else if (dipilih == 3)
                            {
                                system("clear");
                                rak.Kode_Rak = InputKodeRak();
                                Cari_Rak(data_rak, rak.Kode_Rak);   
                            }
                            else
                            {
                                system("clear");
                                break;
                            }
                        }               
                    }
                    else if (dipilih == 4)
                    {
                        break;
                    }              
                }
            }
        }
        else if (dipilih == 3)
        {
            system("clear");
            return -1;
        }  
        cin.get();
        cin.get();
    }   
    return 0;
}