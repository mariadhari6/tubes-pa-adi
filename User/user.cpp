#include "user.h"
#include <time.h>
string getID(){
    string id = "";
    int indexId;
    for (int i = 0; i < 12; i++)
    {
        indexId = rand() % 26;
        id = id + idGenerator[indexId];
    }
    return id;
}
string InputNamaUser(){
    string nama;
    cin.get();
    cout<<"Nama\t: ";
    cin.get(Input, 90);
    nama = Input;
    return nama;
}
string InputJKUser(){
    int i;
    string jk;
    do
    {
        cout<<"Jenis Kelamin\t: \n";
        for (i = 0; i < 2; i++)
        {
            cout<<"\t\t: "<<i+1<<". "<<jk_user[i]<<endl;
        }
        cout<<"  Pilih: ";
        cin>>jk;
        try
        {
            i = stoi(jk);
        }
        catch(const std::exception& e)
        {
            i = 0;
        }
    } while (i<1 || i>2);
    jk = jk_user[i-1];
    return jk;
}
string InputAlamatUser(){
    string alamat;
    cin.get();
    cout<<"Alamat\t: ";
    cin.get(Input, 90);
    alamat = Input;
    return alamat;   
}

string InputUsername(){
    string username;
    int i=0;
    bool detekSpasi = false;
    char huruf;
    do
    {
        cin.get();
        cout<<"Username\t: ";
        cin.get(Input, 90);
        while (Input[i])
        {
            detekSpasi = false;
            huruf = Input[i];
            if (isspace(huruf))
            {
                detekSpasi = true;
                cout<<"Jangan Gunakan Spasi !!!\n";
                break;
            }
            i++;
        }
    } while (detekSpasi == true);
    username = Input;
    return username;
}
bool CekID(list_user l, string id){
    address2 a = l.mulai;
    while (a!=NULL)
    {
        if (a->info.id_petugas.compare(id) == 0)
        {
            return true;
        }
        a = a->next;
    }
    return false;
}

bool CekUsername(list_user l, string username){
    address2 a =l.mulai;
    while (a!=NULL)
    {
        if (a->info.username.compare(username) == 0)
        {
            return true;
        }
        
        a = a->next;
    }
    return false;
}

string InputNoHPUser(){
    string no_hp;
    char _no_hp[100];
    
    int hp_int, i=0;
    bool salah = false;
    string test;
    do
    {
        cin.get();
        salah = false;
        cout<<"No HP\t: ";
        cin.get(_no_hp, 90);
        no_hp = _no_hp;
        while (i<no_hp.size())
        {
            test = _no_hp[i];
            try
            {
                hp_int = stoi(test);
            }
            catch(const std::exception& e)
            {
                salah = true;
            }
            if (salah == true)
            {
                break;
            }
            
            i++;
        }
    } while (salah == true);
    
    
    cout<<endl;
    no_hp.resize(13);
    return no_hp;   
}
string InputPassword(){
    string password;
    cin.get();
    cout<<"Password\t: ";
    cin.get(Input, 90);
    password = Input;
    return password;
}
address2 Alokasi_User(string id, string nama, string jk,
                      string alamat, string no_telp,
                      string username, string password){
    address2 a = new elemen_user;
    a->info.id_petugas = id;
    a->info.nama_petugas = nama;
    a->info.jk = jk;
    a->info.alamat = alamat;
    a->info.no_telp = no_telp;
    a->info.username = username;
    a->info.password = password;
    return a;
}
list_user Tambah_User(list_user l, address2 data_list){
    if (l.mulai == NULL)
    {
        l.mulai = data_list;
        return l;
    }
    data_list->next = l.mulai;
    l.mulai = data_list;
    data_list = NULL;
    return l;
}
void Tampilkan_User(list_user l){
    address2 a = l.mulai;
    while (a!=NULL)
    {
        cout<<"username : "<<a->info.username<<endl;
        a = a->next;
    }   
}
bool CekPassword(list_user l, string username, string password){
    address2 a = l.mulai;
    bool _status_username = false, _status_password = false;
    while (a!=NULL)
    {
        _status_username = false;
        _status_password = false;
        if (a->info.username.compare(username) == 0)
        {
            _status_username = true;
        }
        if (a->info.password.compare(password) == 0)
        {
            _status_password = true;
        }
        if (_status_username == true && _status_password ==true)
        {
            return true;
        }
        a = a->next;
    }
    return false;
}