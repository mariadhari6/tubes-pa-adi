#include<iostream>
#include<curses.h>
#include<string>

#ifndef USER_LIBRARY
#define USER_LIBRARY

using namespace std;

typedef struct elemen_user *address2;
struct user
{
    string id_petugas, nama_petugas, jk;
    string alamat, no_telp, username, password;
};
struct elemen_user
{
    user info;
    address2 next;
};
struct list_user
{
    address2 mulai;
};
list_user data_user;
const char idGenerator[] = {
    'A', 'B', 'C', 'D', 'E', 'F', 'G',
    'H', 'I', 'J', 'K', 'L', 'M', 'N',
    'O', 'P', 'Q', 'R', 'S', 'T', 'U',
    'V', 'W', 'X', 'Y', 'Z', 'a', 'b',
    'c', 'd', 'e', 'f', 'g', 'h', 'i',
    'j', 'k', 'l', 'm', 'n', 'o', 'p',
    'q', 'r', 's', 't', 'u', 'v', 'w',
    'x', 'y', 'z', '1', '2', '3', '4',
    '5', '6', '7', '8', '9', '0'
};
char Input[100];
string jk_user[2] = {
    "Laki-laki",
    "Perempuan"
};
string getID();
string InputNamaUser();
string InputJKUser();
string InputAlamatUser();
string InputNoHPUser();
string InputUsername();
string InputPassword();
bool CekID(list_user l, string id);
bool CekUsername(list_user l, string username);
address2 Alokasi_User(string id, string nama, string jk,
                      string alamat, string no_telp,
                      string username, string password);
list_user Tambah_User(list_user l, address2 data_list);
void Tampilkan_User(list_user l);
bool CekPassword(list_user l, string username, string password);
#endif