// Database Perpustakaan
#include<iostream>
#include<curses.h>
//#include<conio.h> untuk wndows

#include<string>
#include "../Rak/rak.h"

#ifndef BOOK_LIBRARY
#define BOOK_LIBRARY

using namespace std;
typedef struct elemen_buku *address;
struct buku
{
    string judul, id_buku, kode_rak;
    int thn_terbit, cetakan_ke;
    string penulis, penerbit, kota_terbit, jenis_buku;
    int jml_genre;
    string genre[10];
};
struct elemen_buku
{
    buku info;
    address next;
};
struct list_buku
{
    address mulai;
};
string dataBuku;
char Masukkan[100];
string m;
string genre_fiksi[21] = {
    "Romance", "Action Adventure", "Science Fiction",
    "Fantasy", "Speculative Fiction", "Suspense",
    "Young Adult", "New Adult", "Horror",
    "Thriller", "Paranormal", "Ghost",
    "Mystery", "Crime", "Police Procedurals",
    "Historical", "Western", "Family Saga",
    "Women’s Fiction", "Magic Realism", "Literary Fiction"
};
string genre_nonfiksi[20] = {
    "Makalah Akademik", "Penerbitan Akademik",
    "Otobiografi", "Biografi", "Cetak Biru",
    "Laporan Buku", "Buku Harian", "Kamus",
    "Panduan dan Manual", "Buku Pedoman", "Sejarah",
    "Jurnal", "Kritik Sastra", "Filsafat",
    "Fotografi", "Travelog", "Buku Ilmiah",
    "Sains Populer", "Esai", "Almanak"
};
void Buat_Data_Buku(list_buku Data);
string InputIDBuku();
string InputJudul();
string InputPenulis();
string InputRak(list_rak l);
string InputJenisBuku();
string InputGenre(string jns_buku);
int TahunTerbit();
string InputPenerbit();
int InputCetakan();
string InputKota();
bool CekID(list_buku l, string id_tujuan);
bool CekJudul(list_buku l, string judul);
int Pilihan_Buku();
address Alokasi(string Judul, string ID_Buku, string Kode_rak,
                int Thn_terbit, int Cetakan_ke,
                string Penulis, string Penerbit, string Kota_terbit,
                string Jenis_Buku, string Genre[], int i);
list_buku Tambah_Awal(list_buku l, address data_list);
list_buku Tambah_Akhir(list_buku l, address data_list);
list_buku Tambah_Setelah(list_buku l, address data_list, string id_tujuan);
list_buku Delete_Buku(list_buku l, string id_tujuan, list_rak r);
list_buku Delete_Buku_Setelah(list_buku l, string id_tujuan, list_rak r);
list_buku Update_Data(list_buku l, address data_list, string id_tujuan, int i);
void Tampilkan_Data(list_buku l);
void Cari_Buku(list_buku l, string Judul);
void Hitung_Buku(list_buku l);
int InputIsiMax();
void Cari_Buku_Genre(list_buku l, string genre);
bool CekPenerbit(list_buku l, string penerbit);
bool CekPenulis(list_buku l, string penulis);
void Cari_Penerbit(list_buku l, string penerbit);
void Cari_Penulis(list_buku l, string penulis);
string UpdateRak(list_buku l, list_rak r, string id_tujuan);
list_buku data_buku;
#endif