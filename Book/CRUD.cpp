#include "book.h"

address Alokasi(string Judul, string ID_Buku, string Kode_rak,
                int Thn_terbit, int Cetakan_ke,
                string Penulis, string Penerbit, string Kota_terbit,
                string Jenis_Buku, string Genre[], int i){
    
    address a=new elemen_buku;
    a->info.id_buku=ID_Buku;
    a->info.judul=Judul;
    a->info.kode_rak=Kode_rak;
    a->info.thn_terbit=Thn_terbit;
    a->info.cetakan_ke=Cetakan_ke;
    a->info.penulis=Penulis;
    a->info.penerbit=Penerbit;
    a->info.kota_terbit=Kota_terbit;
    a->info.jenis_buku=Jenis_Buku;
    for(int b=0; b<i+1; b++){
        a->info.genre[b]=Genre[b];
    }
    a->info.jml_genre=i+1;
    a->next=NULL;
    return a;
}

list_buku Tambah_Awal(list_buku l, address data_list){
    if (l.mulai==NULL)
    {
        l.mulai=data_list;
        return l;
    }
    else
    {
        data_list->next=l.mulai;
        l.mulai=data_list;
        data_list=NULL;
        free(data_list);
        return l;
    }
}

list_buku Tambah_Akhir(list_buku l, address data_list){
    address a=l.mulai;
    while (a->next!=NULL)
    {
        a=a->next;
    }
    a->next=data_list;
    data_list=NULL;
    return l;
}

list_buku Delete_Buku_Setelah(list_buku l, string id_tujuan, list_rak r){
    address wadah;
    address1 b = r.mulai;
    address a=l.mulai;
    if (l.mulai->info.id_buku.compare(id_tujuan) == 0)
    {
        if (l.mulai->next == NULL)
        {
            system("clear");
            cout<<"Data Setelah ID Buku "<<id_tujuan<<" Masih Kosong !!!\n";
            return l;
        }   
        wadah=a->next;
        while (b!=NULL)
        {
            if (b->info.kode_rak.compare(wadah->info.kode_rak)==0)
            {
                b->info.isi--;
            }
            
            b = b->next;
        }
        l.mulai->next=wadah->next;
        return l;
    }
    
    b = r.mulai;
    while (a->next!=NULL)
    {
        if (a->info.id_buku.compare(id_tujuan)==0)
        {
            wadah=a->next;
            while (b!=NULL)
            {
                if (b->info.kode_rak.compare(wadah->info.kode_rak)==0)
                {
                    b->info.isi--;
                }
                b = b->next;
            }
            a->next = wadah->next;
            return l;
        }
        a=a->next;
    }
    system("clear");
    cout<<"Data Setelah ID Buku "<<id_tujuan<<" Masih Kosong !!!\n";
    return l;
}
list_buku Delete_Buku(list_buku l, string id_tujuan, list_rak r){
    address wadah;
    address1 b = r.mulai;
    address a=l.mulai;
    if (l.mulai->info.id_buku.compare(id_tujuan) == 0)
    {
        wadah=l.mulai;
        l.mulai=l.mulai->next;
        while (b!=NULL)
        {
            if (b->info.kode_rak.compare(a->info.kode_rak)==0)
            {
                b->info.isi--;
            }
            
            b = b->next;
        }
        return l;
    }
    
    b = r.mulai;
    while (a->next!=NULL)
    {
        if (a->next->info.id_buku.compare(id_tujuan)==0)
        {
            wadah=a->next;
            a->next=wadah->next;
            while (b!=NULL)
            {
                if (b->info.kode_rak.compare(a->info.kode_rak)==0)
                {
                    b->info.isi--;
                }
                
                b = b->next;
            }
            return l;
        }
        a=a->next;
    }
}

void Tampilkan_Data(list_buku l){
    if (l.mulai==NULL)
    {
        cout<<"Data Buku Kosong !!!\n";
        return;
    }
    address a=l.mulai;
    int x=1;
    while (a!=NULL)
    {
        cout<<"=====Data Buku Ke-"<<x<<"=====\n";
        cout<<"ID Buku\t= "<<a->info.id_buku<<endl;
        cout<<"Judul\t= "<<a->info.judul<<endl;
        cout<<"Kode Rak= "<<a->info.kode_rak<<endl;
        cout<<"Penulis\t= "<<a->info.penulis<<endl;
        cout<<"Jenis\t= "<<a->info.jenis_buku<<endl;
        cout<<"Genre\t= 1. "<<a->info.genre[0]<<endl;
        for (int i = 1; i < a->info.jml_genre; i++)
        {
            cout<<"\t  "<<i+1<<". "<<a->info.genre[i]<<endl;
        }
        cout<<"Penerbit= "<<a->info.penerbit<<endl;
        cout<<"Tahun\t= "<<a->info.thn_terbit<<endl;
        cout<<"Kota\t= "<<a->info.kota_terbit<<endl;
        cout<<"Penulis\t= "<<a->info.penulis<<endl;
        cout<<"Cetakan\t= "<<a->info.cetakan_ke<<endl<<endl;
        x++;
        a=a->next;
    }  
}

void Cari_Buku(list_buku l, string Judul){
    system("clear");
    address a=l.mulai;
    int z = 1;
    while (a!=NULL)
    {
        if (a->info.judul.compare(Judul)==0)
        {
            cout<<"=====Data Buku Ke-"<<z<<"=====\n";
            cout<<"ID Buku\t= "<<a->info.id_buku<<endl;
            cout<<"Judul\t= "<<a->info.judul<<endl;
            cout<<"Kode Rak= "<<a->info.kode_rak<<endl;
            cout<<"Penulis\t= "<<a->info.penulis<<endl;
            cout<<"Jenis\t= "<<a->info.jenis_buku<<endl;
            cout<<"Genre\t= 1. "<<a->info.genre[0]<<endl;
            for (int i = 1; i < a->info.jml_genre; i++)
            {
                cout<<"\t  "<<i+1<<". "<<a->info.genre[i]<<endl;
            }
            cout<<"Penerbit= "<<a->info.penerbit<<endl;
            cout<<"Tahun\t= "<<a->info.thn_terbit<<endl;
            cout<<"Kota\t= "<<a->info.kota_terbit<<endl;
            cout<<"Cetakan\t= "<<a->info.cetakan_ke<<endl<<endl;
            z++;
        }
        a = a->next;
    }
}

void Hitung_Buku(list_buku l){
    int jml=0;
    address a=l.mulai;
    while (a!=NULL)
    {
        a=a->next;
        jml++;
    }
    cout<<"Jumlah Buku Di Perpustakaan : "<<jml<<endl;
}

list_buku Tambah_Setelah(list_buku l, address data_list, string id_tujuan){
    if (l.mulai!=NULL)
    {
        address a=l.mulai;
        while (a!=NULL)
        {
            if (a->info.id_buku.compare(id_tujuan)==0)
            {
                data_list->next=a->next;
                a->next=data_list;
                return l;
            }
            a=a->next;
        }   
    }
}

list_buku Update_Data(list_buku l, address data_list, string id_tujuan, int i){
    address a=l.mulai;
    address1 b = data_rak.mulai;
    while (a!=NULL)
    {
        if (a->info.id_buku.compare(id_tujuan)==0)
        {
            a->info.judul=data_list->info.judul;
            a->info.kode_rak=data_list->info.kode_rak;
            a->info.thn_terbit=data_list->info.thn_terbit;
            a->info.cetakan_ke=data_list->info.cetakan_ke;
            a->info.penulis=data_list->info.penulis;
            a->info.penerbit=data_list->info.penerbit;
            a->info.kota_terbit=data_list->info.kota_terbit;
            a->info.jenis_buku=data_list->info.jenis_buku;
            for(int b=0; b<i+1; b++){
                a->info.genre[b]=data_list->info.genre[b];
            }
            a->info.jml_genre=i+1;
            return l;
        }
        a=a->next;
    }   
}
void Cari_Buku_Genre(list_buku l, string genre){
    address a = l.mulai;
    int x = 1;
    while (a!=NULL)
    {
        for (int i = 0; i<a->info.jml_genre; i++)
        {
            if (a->info.genre[i].compare(genre) == 0)
            {
                cout<<"=====Data Buku Ke-"<<x<<"=====\n";
                cout<<"ID Buku\t= "<<a->info.id_buku<<endl;
                cout<<"Judul\t= "<<a->info.judul<<endl;
                cout<<"Kode Rak= "<<a->info.kode_rak<<endl;
                cout<<"Penulis\t= "<<a->info.penulis<<endl;
                cout<<"Jenis\t= "<<a->info.jenis_buku<<endl;
                cout<<"Genre\t= 1. "<<a->info.genre[0]<<endl;
                for (int z = 1; z<a->info.jml_genre; z++)
                {
                    cout<<"\t  "<<z+1<<". "<<a->info.genre[z]<<endl;
                }
                cout<<"Penerbit= "<<a->info.penerbit<<endl;
                cout<<"Tahun\t= "<<a->info.thn_terbit<<endl;
                cout<<"Kota\t= "<<a->info.kota_terbit<<endl;
                cout<<"Penulis\t= "<<a->info.penulis<<endl;
                cout<<"Cetakan\t= "<<a->info.cetakan_ke<<endl<<endl;
                x++;
            }
        }
        a = a->next;
    }   
}

void Cari_Penerbit(list_buku l, string penerbit){
    address a = l.mulai;
    int x = 1;
    while (a!=NULL)
    {
        if (a->info.penerbit.compare(penerbit) == 0)
        {
            cout<<"=====Data Buku Ke-"<<x<<"=====\n";
            cout<<"ID Buku\t= "<<a->info.id_buku<<endl;
            cout<<"Judul\t= "<<a->info.judul<<endl;
            cout<<"Kode Rak= "<<a->info.kode_rak<<endl;
            cout<<"Penulis\t= "<<a->info.penulis<<endl;
            cout<<"Jenis\t= "<<a->info.jenis_buku<<endl;
            cout<<"Genre\t= 1. "<<a->info.genre[0]<<endl;
            for (int z = 1; z<a->info.jml_genre; z++)
            {
                cout<<"\t  "<<z+1<<". "<<a->info.genre[z]<<endl;
            }
            cout<<"Penerbit= "<<a->info.penerbit<<endl;
            cout<<"Tahun\t= "<<a->info.thn_terbit<<endl;
            cout<<"Kota\t= "<<a->info.kota_terbit<<endl;
            cout<<"Penulis\t= "<<a->info.penulis<<endl;
            cout<<"Cetakan\t= "<<a->info.cetakan_ke<<endl<<endl;
            x++;
        }
        a = a->next;
    }
}

void Cari_Penulis(list_buku l, string penulis){
    address a = l.mulai;
    int x = 1;
    while (a!=NULL)
    {
        if (a->info.penulis.compare(penulis) == 0)
        {
            cout<<"=====Data Buku Ke-"<<x<<"=====\n";
            cout<<"ID Buku\t= "<<a->info.id_buku<<endl;
            cout<<"Judul\t= "<<a->info.judul<<endl;
            cout<<"Kode Rak= "<<a->info.kode_rak<<endl;
            cout<<"Penulis\t= "<<a->info.penulis<<endl;
            cout<<"Jenis\t= "<<a->info.jenis_buku<<endl;
            cout<<"Genre\t= 1. "<<a->info.genre[0]<<endl;
            for (int z = 1; z<a->info.jml_genre; z++)
            {
                cout<<"\t  "<<z+1<<". "<<a->info.genre[z]<<endl;
            }
            cout<<"Penerbit= "<<a->info.penerbit<<endl;
            cout<<"Tahun\t= "<<a->info.thn_terbit<<endl;
            cout<<"Kota\t= "<<a->info.kota_terbit<<endl;
            cout<<"Penulis\t= "<<a->info.penulis<<endl;
            cout<<"Cetakan\t= "<<a->info.cetakan_ke<<endl<<endl;
            x++;
        }
        a = a->next;
    }
}