#include "book.h"
void Buat_Data_Buku(list_buku Data){
    Data.mulai = NULL;
}
int Pilihan_Buku(){
    int pilih;
    cout<<"1.Tampilkan Data Buku\n2.Tambah Buku Diawal\n3.Tambah ";
    cout<<"Buku Diakhir\n4.Delete Buku\n5.Cari Buku\n6.Hitung ";
    cout<<"Jumlah Buku\n7.Tambah Buku Setelah\n8.Update Data Buku";
    cout<<"\n9.Delete Setelah\n10.Kembali\n  Pilih\t: ";
    cin>>dataBuku;
    try
    {
        pilih=stoi(dataBuku);
    }
    catch(const std::exception& e)
    {
        pilih=0;
    }
    return pilih;
}

//Input Data Atau Field
string InputIDBuku(){
    cout<<"ID Buku\t: ";
    cin>>dataBuku;
    return dataBuku;
}

string InputJudul(){
    cin.get();
    cout<<"Judul\t: ";
    cin.get(Masukkan, 90);
    dataBuku=Masukkan;
    return dataBuku;
}

string InputPenulis(){
    cin.get();
    cout<<"Penulis\t: ";
    cin.get(Masukkan, 90);
    dataBuku=Masukkan;
    return dataBuku;
}

string InputRak(list_rak l){
    int pil;
    cin.get();
    bool penuh = false;
    address1 a = l.mulai;
    do
    {
        do
        {
            cout<<"ID Rak\t|\tKode Rak \n";
            while (a!=NULL)
            {
                cout<<a->info.id_rak<<"\t|\t"<<a->info.kode_rak<<endl;
                if (a->next == NULL)
                {
                    break;
                }
                a = a->next;
            }
            cout<<"Pilih\t: ";
            cin>>dataBuku;
            try
            {
                pil = stoi(dataBuku);
            }
            catch(const std::exception& e)
            {
                pil = 0;
            }
        } while (pil<=0 || pil>a->info.id_rak);
        a = l.mulai;
        while (a!=NULL)
        {
            if(pil == a->info.id_rak){
                dataBuku = a->info.kode_rak;
                break;
            }
            a = a->next;
        }
        if (a->info.isi>=a->info.isi_max)
        {
            cout<<"Rak Telah Penuh!!!\n";
            penuh = true;
            a = l.mulai;
        }
        else
        {
            penuh = false;
        }
        
    } while (a->info.isi>=a->info.isi_max || penuh == true);
    a->info.isi++;
    return dataBuku;
}


string InputJenisBuku(){
    int pilih;
    bool eror;
    do{
        eror=false;
        cout<<"Jenis Buku\t: 1. Fiksi\n";
        cout<<"\t\t  2. Non-Fiksi\n";
        cout<<"Pilih: ";
        cin>>dataBuku;
        try
        {
            pilih=stoi(dataBuku);
        }
        catch(const std::exception& e)
        {
            eror=true;
        }
        
    } while (eror==true || (pilih!=1 && pilih!=2));
    if(pilih==1){
        dataBuku="Fiksi";
    }
    else if (pilih==2)
    {
        dataBuku="Non-Fiksi";
    }
    return dataBuku;
}
string InputGenre(string jns_buku){
    int pilih;
    bool gagal_konversi=false;
    cout<<"Genre\t: \n";
    if(jns_buku.compare("Fiksi")==0){
        do{
            gagal_konversi=false;
            for (int  i = 0; i < 21; i++)
            {
                cout<<"\t  "<<i+1<<". "<<genre_fiksi[i]<<endl;
            }
            cout<<"  Pilih\t: ";
            cin>>dataBuku;
            try
            {
                pilih=stoi(dataBuku);
            }
            catch(const std::exception& e)
            {
                gagal_konversi=true;
            }
            dataBuku = genre_fiksi[pilih-1];
        } while (gagal_konversi==true || (pilih>21 || pilih<1));
    }
    else if(jns_buku.compare("Non-Fiksi")==0){
        do{
            gagal_konversi=false;
            for (int  u = 0; u < 20; u++)
            {
                cout<<"\t  "<<u+1<<". "<<genre_nonfiksi[u]<<endl;
            }
            cout<<"  Pilih\t: ";
            cin>>dataBuku;
            try
            {
                pilih=stoi(dataBuku);
            }
            catch(const std::exception& e)
            {
                gagal_konversi=true;
            }
            dataBuku=genre_nonfiksi[pilih-1];
        } while (gagal_konversi==true || (pilih>20 || pilih<1));
    }
    return dataBuku;
}

int TahunTerbit(){
    bool eror;
    int thn_terbit;
    do{
        eror=false;
        cout<<"Tahun Terbit\t: ";
        cin>>dataBuku;
        try
        {
            thn_terbit=stoi(dataBuku);
        }
        catch(const std::exception& e)
        {
            eror=true;
        }
    } while (eror==true || (thn_terbit<1900 || thn_terbit>2020));
    return thn_terbit;
}

string InputPenerbit(){
    cin.get();
    cout<<"Penerbit\t: ";
    cin.getline(Masukkan, 100);
    dataBuku=Masukkan;
    return dataBuku;
}

int InputCetakan(){
    bool eror;
    int cetakan;
    do{
        eror=false;
        cout<<"Cetakan\t\t: ";
        cin.get(Masukkan, 3);
        dataBuku=Masukkan;
        try
        {
            cetakan=stoi(dataBuku);
        }
        catch(const std::exception& e)
        {
            eror = true;
        }
    } while (eror==true);
    return cetakan; 
}

string InputKota(){
    cin.get();
    cout<<"Kota Terbit\t: ";
    cin.get(Masukkan, 100);
    dataBuku=Masukkan;
    return dataBuku;
}

// validasi
bool CekID(list_buku l, string id_tujuan){
    address a=l.mulai;  
    while (a!=NULL)
    {
        if (a->info.id_buku.compare(id_tujuan)==0)
        {
            return true;
        }
        a=a->next;
    }
    return false;   
}
bool CekJudul(list_buku l, string judul){
    address a=l.mulai;
    while (a!=NULL)
    {
        if (a->info.judul.compare(judul) == 0)
        {
            return true;
        }
        a=a->next;
    }
    return false;
}

bool CekPenerbit(list_buku l, string penerbit){
    address a = l.mulai;
    while (a!=NULL)
    {
        if (a->info.penerbit.compare(penerbit) == 0)
        {
            return true;
        }
        
        a = a->next;
    }
    return false;
}

bool CekPenulis(list_buku l, string penulis){
    address a = l.mulai;
    while (a!=NULL)
    {
        if (a->info.penulis.compare(penulis) == 0)
        {
            return true;
        }
        
        a = a->next;
    }
    return false;
}

string UpdateRak(list_buku l, list_rak r, string id_tujuan){
    address a = l.mulai;
    address1 b = r.mulai;

    int pil;
    cin.get();
    bool penuh = false;

    cout<<"ID Rak\t|\tKode Rak \n";    
    do
    {
        do
        {
            while (b != NULL)
            {
                cout<<b->info.id_rak<<"\t|\t"<<b->info.kode_rak<<endl;
                if (b->next == NULL)
                {
                    break;
                }
                b = b->next;
            }
            cout<<"Pilih\t: ";
            cin>>dataBuku;
            try
            {
                pil = stoi(dataBuku);
            }
            catch(const std::exception& e)
            {
                pil = 0;
            }
        } while (pil<=0 || pil>b->info.id_rak);
        b = r.mulai;
        while (b != NULL)
        {
            if (pil == b->info.id_rak)
            {
                dataBuku = b->info.kode_rak;
                break;
            }
            b = b->next;
        }
        if (b->info.isi>=b->info.isi_max)
        {
            cout<<"Rak Telah Penuh!!!\n";
            penuh = true;
            b = r.mulai;
        }
        else
        {
            penuh = false;
        }
    } while (b->info.isi>=b->info.isi_max || penuh == true);
    b = r.mulai;
    while (a != NULL)
    {
        if (a->info.id_buku.compare(id_tujuan) == 0)
        {
            while (b != NULL)
            {
                if (b->info.kode_rak.compare(a->info.kode_rak) == 0)
                {
                    b->info.isi--;
                    break;
                }
                b = b->next;
            }
            break;
        }
        a = a->next;
    }
    b = r.mulai;
    while (b != NULL)
    {
        if (b->info.kode_rak.compare(dataBuku) == 0)
        {
            b->info.isi++;
        }
        b = b->next;
    }
    return dataBuku;
}