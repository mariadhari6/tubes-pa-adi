#include "Member/member.cpp"

class DataAnggota
{
    public:
        string nim_anggota, nama_anggota, jk_anggota;
        string alamat, no_telp, kelas, nim_tujuan;
        int thn_masuk;
        bool sama = false;
        void Input_Data(){
            do
            {
                nim_anggota = InputNim();
                sama = CekNim(data_anggota, nim_anggota);
                if (sama == true)
                {
                    cout<<"NIM Sudah Terdaftar!!!\n";
                }
            } while (sama == true);
            nama_anggota =  InputNama();
            kelas = InputKelas();
            jk_anggota = InputJKAnggota();
            alamat = InputAlamat();
            no_telp = InputNoHP();
            thn_masuk = InputTahunMasuk();
        }
        void Update(){
            nama_anggota =  InputNama();
            kelas = InputKelas();
            jk_anggota = InputJKAnggota();
            alamat = InputAlamat();
            no_telp = InputNoHP();
            thn_masuk = InputTahunMasuk();
        }
        int PilihTampilkan(){
            string pil;
            int p;
            do
            {
                system("clear");
                cout<<"1. Semua\n2. Cari Nama\n3, ";
                cout<<"Cari Kelas\n4. kembali\n  Pilih : ";
                cin>>pil;
                try
                {
                    p = stoi(pil);
                }
                catch(const std::exception& e)
                {
                    p = 0;
                }
            } while (p<=0 || p>4);
            return p;
        }
};
