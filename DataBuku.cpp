#include "Book/book.cpp"
//Data struct buku

class DataBuku{
    public:
        string Judul, ID_Buku, Kode_rak;
        int Thn_terbit, Cetakan_ke;
        string Penulis, Penerbit, Kota_terbit, Jenis_Buku;
        string Genre[10], genre;
        string judul_tujuan, id_tujuan;
        bool sama;
        int i;

        void Input_Data(){
            i = 0;
            sama=false;
            char tambah;
            cin.get();
            do
            {
                ID_Buku=InputIDBuku();
                if (ID_Buku.length()>3)
                {
                    ID_Buku.resize(3);
                }
                sama=CekID(data_buku, ID_Buku);
            } while (sama==true);
            Judul=InputJudul();
            Penulis=InputPenulis();
            Kode_rak=InputRak(data_rak);
            Jenis_Buku=InputJenisBuku();
            Genre[0]=InputGenre(Jenis_Buku);
            cout<<"Tambah genre?(y): ";
            cin>>tambah;
            if (tambah == 'y' ||tambah == 'Y')
            {
                for (i = 1; i < 10; i++)
                {
                    tambah='n';
                    do{
                        Genre[i]=InputGenre(Jenis_Buku);
                        for (int z = 0; z<i; z++)
                        {
                            if (Genre[i].compare(Genre[z])==0)
                            {
                                sama=true;
                                break;
                            }
                            else
                            {
                                sama=false;
                            }
                        }
                    } while (sama==true);
                    if (i<9)
                    {
                        cout<<"Tambah genre?(y): ";
                        cin>>tambah;
                    }
                    if (tambah!='y')
                    {
                        break;
                    }
                }
            }
            Thn_terbit=TahunTerbit();
            Penerbit=InputPenerbit();
            Cetakan_ke=InputCetakan();
            Kota_terbit=InputKota();
        }

        void Update(){
            i = 0;
            sama=false;
            char tambah;
            cin.get();
            system("clear");
            Judul=InputJudul();
            Penulis=InputPenulis();
            Kode_rak=UpdateRak(data_buku, data_rak, id_tujuan);
            Jenis_Buku=InputJenisBuku();
            Genre[0]=InputGenre(Jenis_Buku);
            cout<<"Tambah genre?(y): ";
            cin>>tambah;
            if (tambah == 'y' ||tambah == 'Y')
            {
                for (i = 1; i < 10; i++)
                {
                    tambah='n';
                    do{
                        Genre[i]=InputGenre(Jenis_Buku);
                        for (int z = 0; z<i; z++)
                        {
                            if (Genre[i].compare(Genre[z])==0)
                            {
                                sama=true;
                                break;
                            }
                            else
                            {
                                sama=false;
                            }
                        }
                    } while (sama==true);
                    if (i<9)
                    {
                        cout<<"Tambah genre?(y): ";
                        cin>>tambah;
                    }
                    if (tambah!='y')
                    {
                        break;
                    }
                }
            }
            Thn_terbit=TahunTerbit();
            Penerbit=InputPenerbit();
            Cetakan_ke=InputCetakan();
            Kota_terbit=InputKota();
        }
        int PilihTampilkan(){
            string pil;
            int p;
            do
            {
                system("clear");
                cout<<"1. Semua\n2. Genre\n3. Penerbit\n4. Penulis";
                cout<<"\n5. Kembali\n  Pilih : ";
                cin>>pil;
                try
                {
                    p = stoi(pil);
                }
                catch(const std::exception& e)
                {
                    p = 0;
                }
            } while (p<=0 || p>6);
            return p;
        }
};